﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Threading;


namespace Lab_2
{
    public partial class task3 : Form
    {

        #region Fields

        /// <summary>
        /// Длина массива
        /// </summary>
        public static int ArrayLength { get; set; }
        /// <summary>
        /// Клик по вводу
        /// </summary>
        public bool IsInputBtnClick { get; set; }
        /// <summary>
        /// Клик по выводу
        /// </summary>
        public bool IsOutpitBtnClick { get; set; }
        /// <summary>
        /// Нумерация элементов массива
        /// </summary>
        public int ArrayCount { get; set; }
        /// <summary>
        /// Массив значений
        /// </summary>
        public List<int> MyList { get; set; }

        #endregion

        public task3()
        {
            InitializeComponent();
            ArrayCount = 1;
        }

        private void task3_Load(object sender, EventArgs e)
        {
            btnOutput.Select();
        }

        /// <summary>
        /// Событие на нажатие пуск
        /// </summary>
        private void btnOutput_Click(object sender, EventArgs e)
        {
            if (!IsOutpitBtnClick)
            {
                IsOutpitBtnClick = true;
                btnInput.Enabled = true;
                txtInput.Enabled = true;
                lblInput.Text = @"Введите количество элеметнов массива";
                btnOutput.Text = @"Вывод массива";
                txtInput.Select();
            }
            else
            {
                txtOutput.Clear();
                if (MyList != null && MyList.Any())
                {
                    foreach (var item in MyList)
                    {
                        txtOutput.Text = txtOutput.Text + " " + item.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Событие нажатия на ввод
        /// </summary>
        private void btnInput_Click(object sender, EventArgs e)
        {
            AddList();
        }

        /// <summary>
        /// Метод добавления значений в массив
        /// </summary>
        public void AddList()
        {
            try
            {
                if (string.IsNullOrEmpty(txtInput.Text))
                {
                    MessageBox.Show(@"Введите значение");

                    return;
                }

                if (Convert.ToInt32(txtInput.Text) == 0)
                {
                    MessageBox.Show(@"Длина массива должна быть больше 1");

                    return;
                }

                if (!IsInputBtnClick)
                {
                    IsInputBtnClick = true;
                    ArrayLength = Convert.ToInt32(txtInput.Text);
                    MyList = new List<int>(ArrayLength);
                }
                else
                {
                    MyList.Add(Convert.ToInt32(txtInput.Text));
                    if (ArrayLength == MyList.Count)
                    {
                        lblInput.Text = @"Ввод завершен";
                        txtInput.Clear();
                        txtInput.Enabled = false;
                        btnInput.Enabled = false;
                        btnOutput.Select();

                        return;
                    }
                }

                DisplayInputText(ArrayCount++);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Произошла ошибка!", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Метод отображения сообщений в лейбле
        /// </summary>
        /// <param name="count">Число элемента массива</param>
        public void DisplayInputText(int count)
        {
            lblInput.Text = string.Format("Введите {0}-ый элемент массива.", count);
            txtInput.Clear();
            txtInput.Select();
        }

        /// <summary>
        /// Подверждения ввода по кнопке "Enter"
        /// </summary>
        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddList();
            }
        }
    }
}

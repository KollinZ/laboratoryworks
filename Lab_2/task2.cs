﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_2
{
    public partial class task2 : Form
    {
        public task2()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработчик кнопки "включить свет"
        /// </summary>
        private void btnLightOn_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            pnlWindow.BackColor = Color.Gold;
        }

        /// <summary>
        /// Обработчик кнопки "выключить свет"
        /// </summary>
        private void btnLightOff_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            pnlWindow.BackColor = SystemColors.Window;
        }

        /// <summary>
        /// Обработчик кнопки мигания
        /// </summary>
        private void btnFlashingLight_Click(object sender, EventArgs e)
        {
            timer.Enabled = !timer.Enabled;
        }

        /// <summary>
        /// обработчик таймера
        /// </summary>
        private void timer_Tick(object sender, EventArgs e)
        {
            pnlWindow.BackColor = pnlWindow.BackColor == Color.Gold ? SystemColors.Control : Color.Gold;
        }
    }
}

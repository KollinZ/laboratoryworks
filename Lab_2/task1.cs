﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_2
{
    public partial class task1 : Form
    {
        public task1()
        {
            InitializeComponent();
        }

        private void btnSum_Click(object sender, EventArgs e)
        {
            CalcArg(true);
        }

        private void btnPro_Click(object sender, EventArgs e)
        {
            CalcArg(false);
        }

        /// <summary>
        /// Суммирование и произведение
        /// </summary>
        /// <param name="type">true - sum | false - pro</param>
        private void CalcArg(bool type)
        {
            if (string.IsNullOrEmpty(txtA.Text) || string.IsNullOrEmpty(txtB.Text))
            {
                MessageBox.Show(@"Число А и Б должны быть заполнены");
            }
            else
            {
                txtResult.Clear();
                txtResult.Text = type ? (Convert.ToInt32(txtA.Text) + Convert.ToInt32(txtB.Text)).ToString() : (Convert.ToInt32(txtA.Text) * Convert.ToInt32(txtB.Text)).ToString();
            }
        }

    }
}

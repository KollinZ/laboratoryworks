﻿namespace Lab_3
{
    partial class ClientTwo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartProcess = new System.Windows.Forms.Button();
            this.lblResultServer = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.lblClientTwoDescription = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStartProcess
            // 
            this.btnStartProcess.Location = new System.Drawing.Point(105, 64);
            this.btnStartProcess.Name = "btnStartProcess";
            this.btnStartProcess.Size = new System.Drawing.Size(116, 45);
            this.btnStartProcess.TabIndex = 6;
            this.btnStartProcess.Text = "Запуск процесса 1 на сервере";
            this.btnStartProcess.UseVisualStyleBackColor = true;
            this.btnStartProcess.Click += new System.EventHandler(this.btnStartProcess_Click);
            // 
            // lblResultServer
            // 
            this.lblResultServer.AutoSize = true;
            this.lblResultServer.Location = new System.Drawing.Point(103, 129);
            this.lblResultServer.Name = "lblResultServer";
            this.lblResultServer.Size = new System.Drawing.Size(127, 13);
            this.lblResultServer.TabIndex = 5;
            this.lblResultServer.Text = "Сообщение от сервера:";
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(36, 149);
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(265, 20);
            this.txtResult.TabIndex = 4;
            // 
            // lblClientTwoDescription
            // 
            this.lblClientTwoDescription.AutoSize = true;
            this.lblClientTwoDescription.Location = new System.Drawing.Point(12, 9);
            this.lblClientTwoDescription.Name = "lblClientTwoDescription";
            this.lblClientTwoDescription.Size = new System.Drawing.Size(310, 39);
            this.lblClientTwoDescription.TabIndex = 7;
            this.lblClientTwoDescription.Text = "Клиент 2 может:\r\n1. Запустить процесс 1 объекта-сервера;\r\n2. Обработать сообщение" +
    ", пришедшее от объекта-сервера.";
            // 
            // ClientTwo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 183);
            this.Controls.Add(this.lblClientTwoDescription);
            this.Controls.Add(this.btnStartProcess);
            this.Controls.Add(this.lblResultServer);
            this.Controls.Add(this.txtResult);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ClientTwo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Клиент № 2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartProcess;
        private System.Windows.Forms.Label lblResultServer;
        private System.Windows.Forms.Label lblClientTwoDescription;
        public System.Windows.Forms.TextBox txtResult;
    }
}
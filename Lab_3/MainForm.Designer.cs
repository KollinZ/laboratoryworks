﻿namespace Lab_3
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
this.btnStartClientOne = new System.Windows.Forms.Button();
this.btnStartClientTwo = new System.Windows.Forms.Button();
this.btnExit = new System.Windows.Forms.Button();
this.SuspendLayout();
// 
// btnStartClientOne
// 
this.btnStartClientOne.Location = new System.Drawing.Point(12, 12);
this.btnStartClientOne.Name = "btnStartClientOne";
this.btnStartClientOne.Size = new System.Drawing.Size(137, 41);
this.btnStartClientOne.TabIndex = 0;
this.btnStartClientOne.Text = "Запустить клиент № 1";
this.btnStartClientOne.UseVisualStyleBackColor = true;
this.btnStartClientOne.Click += new System.EventHandler(this.btnStartClientOne_Click);
// 
// btnStartClientTwo
// 
this.btnStartClientTwo.Location = new System.Drawing.Point(155, 12);
this.btnStartClientTwo.Name = "btnStartClientTwo";
this.btnStartClientTwo.Size = new System.Drawing.Size(137, 41);
this.btnStartClientTwo.TabIndex = 1;
this.btnStartClientTwo.Text = "Запустить клиент № 2";
this.btnStartClientTwo.UseVisualStyleBackColor = true;
this.btnStartClientTwo.Click += new System.EventHandler(this.btnStartClientTwo_Click);
// 
// btnExit
// 
this.btnExit.Location = new System.Drawing.Point(106, 59);
this.btnExit.Name = "btnExit";
this.btnExit.Size = new System.Drawing.Size(83, 39);
this.btnExit.TabIndex = 2;
this.btnExit.Text = "Выход";
this.btnExit.UseVisualStyleBackColor = true;
this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
// 
// MainForm
// 
this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
this.ClientSize = new System.Drawing.Size(306, 110);
this.Controls.Add(this.btnExit);
this.Controls.Add(this.btnStartClientTwo);
this.Controls.Add(this.btnStartClientOne);
this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
this.MaximizeBox = false;
this.Name = "MainForm";
this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
this.Text = "Управляющий объект";
this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStartClientOne;
        private System.Windows.Forms.Button btnStartClientTwo;
        private System.Windows.Forms.Button btnExit;
    }
}


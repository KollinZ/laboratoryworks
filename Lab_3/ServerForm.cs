﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_3
{
    public partial class ServerForm : Form
    {
        
        public ServerForm()
        {
            InitializeComponent();
        }

        private void ServerForm_Load(object sender, EventArgs e)
        {
            MainForm.ListnerThread = new Thread(new ThreadStart(ServerTcp.Create));
            MainForm.ListnerThread.Start();
        }

        private void ServerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                ServerTcp.Server.Stop();
                MainForm.ListnerThread.Suspend();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

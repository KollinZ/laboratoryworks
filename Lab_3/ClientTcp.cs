﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;


namespace Lab_3
{
    public class ClientTcp
    {
        public ClientTcp()
        {
            Connect("127.0.0.1");
        }

        static void Connect(String server)
        {
            try
            {
                // Создаём TcpClient.
                // Для созданного в предыдущем проекте TcpListener 
                // Настраиваем его на IP нашего сервера и тот же порт.
                Int32 port = 9595;
                TcpClient client = new TcpClient(server, port);
                // Переводим наше сообщение в ASCII, а затем в массив Byte.
                Byte[] data = System.Text.Encoding.ASCII.GetBytes("progress");

                // Получаем поток для чтения и записи данных.
                NetworkStream stream = client.GetStream();

                // Отправляем сообщение нашему серверу. 
                stream.Write(data, 0, data.Length);

                // Буфер для хранения принятого массива bytes.
                data = new Byte[256];

                // Строка для хранения полученных ASCII данных.
                String responseData = String.Empty;

                // Читаем первый пакет ответа сервера. 
                // Можно читать всё сообщение.
                // Для этого надо организовать чтение в цикле как на сервере.
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                ClientOne oneForm = (ClientOne)Application.OpenForms["ClientOne"];
                ClientTwo twoForm = (ClientTwo)Application.OpenForms["ClientTwo"];

                if (oneForm != null)
                {
                    oneForm.txtResult.Text = string.IsNullOrEmpty(responseData)
                        ? @"Процесс 1 не удалось запустить"
                        : @"Процесс 1 успешно запущен";
                }

                if (twoForm != null)
                {
                    twoForm.txtResult.Text = string.IsNullOrEmpty(responseData)
                        ? @"Процесс 1 не удалось запустить"
                        : @"Процесс 1 успешно запущен";
                }

                // Закрываем всё.
                stream.Close();
                client.Close();
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
        }
    }
}

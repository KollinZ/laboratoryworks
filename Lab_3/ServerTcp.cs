﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;


namespace Lab_3
{
    public static class ServerTcp
    {
        public static TcpListener Server { get; set; }

        public static void Create()
        {
            try
            {
                // Определим нужное максимальное количество потоков
                // Пусть будет по 4 на каждый процессор
                int MaxThreadsCount = Environment.ProcessorCount * 4;
                Console.WriteLine(MaxThreadsCount.ToString());
                // Установим максимальное количество рабочих потоков
                ThreadPool.SetMaxThreads(MaxThreadsCount, MaxThreadsCount);
                // Установим минимальное количество рабочих потоков
                ThreadPool.SetMinThreads(2, 2);


                // Устанавливаем порт для TcpListener = 9595.
                Int32 port = 9595;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");
                Server = new TcpListener(localAddr, port);

                // Запускаем TcpListener и начинаем слушать клиентов.
                Server.Start();

                // Принимаем клиентов в бесконечном цикле.
                while (true)
                {
                    // При появлении клиента добавляем в очередь потоков его обработку.
                    ThreadPool.QueueUserWorkItem(ObrabotkaZaprosa, Server.AcceptTcpClient());
                }
            }
            catch (SocketException e)
            {
                //В случае ошибки, выводим что это за ошибка.
                MessageBox.Show(string.Format("SocketException: {0}", e));
            }
            finally
            {
                // Останавливаем TcpListener.
                if (Server != null)
                    Server.Stop();
            }
        }

        static async void ObrabotkaZaprosa(object client_obj)
        {
            // Буфер для принимаемых данных.
            Byte[] bytes = new Byte[256];

            TcpClient client = client_obj as TcpClient;

            // Получаем информацию от клиента
            NetworkStream stream = client.GetStream();

            int i;

            // Принимаем данные от клиента в цикле пока не дойдём до конца.
            while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                new Thread(
                    () =>
                    {
                        ServerForm sForm = (ServerForm)Application.OpenForms["ServerForm"];

                        if (sForm.InvokeRequired)
                        {
                            sForm.Invoke(new Action(() => sForm.lblProcessStatus.Text = "Процесс 1 выполнен"));
                            sForm.Invoke(new Action(() => sForm.progBar.Value = 0));
                            sForm.Invoke(new Action(sForm.progBar.PerformStep));
                        }
                    }
                ).Start();

                byte[] msg = System.Text.Encoding.ASCII.GetBytes("1");

                // Отправляем данные обратно клиенту (ответ).
                stream.Write(msg, 0, msg.Length);
            }

            // Закрываем соединение.
            client.Close();
        }
    }
}

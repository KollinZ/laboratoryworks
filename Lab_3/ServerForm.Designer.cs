﻿namespace Lab_3
{
    partial class ServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDecription = new System.Windows.Forms.Label();
            this.lblProcessStatus = new System.Windows.Forms.Label();
            this.progBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // lblDecription
            // 
            this.lblDecription.AutoSize = true;
            this.lblDecription.Location = new System.Drawing.Point(12, 9);
            this.lblDecription.Name = "lblDecription";
            this.lblDecription.Size = new System.Drawing.Size(420, 39);
            this.lblDecription.TabIndex = 0;
            this.lblDecription.Text = "Сервер может:\r\n1. Выполнить процесс 1, и после чего,\r\n2. Послать сообщение об око" +
    "нчании процесса 1 тому, кто создал объект-сервер";
            // 
            // lblProcessStatus
            // 
            this.lblProcessStatus.AutoSize = true;
            this.lblProcessStatus.Location = new System.Drawing.Point(154, 74);
            this.lblProcessStatus.Name = "lblProcessStatus";
            this.lblProcessStatus.Size = new System.Drawing.Size(145, 13);
            this.lblProcessStatus.TabIndex = 1;
            this.lblProcessStatus.Text = "Процесс 1 не выполняется";
            // 
            // progBar
            // 
            this.progBar.Location = new System.Drawing.Point(57, 100);
            this.progBar.Name = "progBar";
            this.progBar.Size = new System.Drawing.Size(342, 23);
            this.progBar.Step = 100;
            this.progBar.TabIndex = 2;
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 153);
            this.Controls.Add(this.progBar);
            this.Controls.Add(this.lblProcessStatus);
            this.Controls.Add(this.lblDecription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServerForm";
            this.Text = "Сервер";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServerForm_FormClosed);
            this.Load += new System.EventHandler(this.ServerForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDecription;
        public System.Windows.Forms.ProgressBar progBar;
        public System.Windows.Forms.Label lblProcessStatus;
    }
}
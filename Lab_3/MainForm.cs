﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_3
{
    public partial class MainForm : Form
    {
        public static Thread ListnerThread { get; set; }

        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// закрываем приложение
        /// </summary>
        private void btnExit_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        /// <summary>
        /// Запускаем клиент 1
        /// </summary>
        private void btnStartClientOne_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ClientOne"] == null)
            {
                Form client = new ClientOne();
                client.Show();
            }
            else
            {
                Application.OpenForms["ClientOne"].Focus();
            }
        }

        /// <summary>
        /// Запускаем клиент 2
        /// </summary>
        private void btnStartClientTwo_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ClientTwo"] == null)
            {
                Form client = new ClientTwo();
                client.Show();
            }
            else
            {
                Application.OpenForms["ClientTwo"].Focus();
            }
        }

        /// <summary>
        /// При закрытии формы, приложение полностью завершается
        /// </summary>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseForm();
        }

        private void CloseForm()
        {
            if (ServerTcp.Server != null)
            {
                ServerTcp.Server.Stop();
            }
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace Lab_3
{
    public partial class ClientOne : Form
    {

        public ClientOne()
        {
            InitializeComponent();
        }

        private void btnStartSever_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ServerForm"] == null)
            {
                Form sForm = new ServerForm();
                sForm.Show();
            }
            else
            {
                MessageBox.Show("Сервер для текущей сессии уже запущен!", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnStartProcess_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ServerForm"] != null)
            {
                ClientTcp client = new ClientTcp();
            }
            else
            {
                MessageBox.Show("Сервер не запущен!", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

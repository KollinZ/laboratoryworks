﻿namespace Lab_3
{
    partial class ClientOne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtResult = new System.Windows.Forms.TextBox();
            this.lblResultServer = new System.Windows.Forms.Label();
            this.btnStartProcess = new System.Windows.Forms.Button();
            this.btnStartSever = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(12, 105);
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(237, 20);
            this.txtResult.TabIndex = 0;
            // 
            // lblResultServer
            // 
            this.lblResultServer.AutoSize = true;
            this.lblResultServer.Location = new System.Drawing.Point(65, 85);
            this.lblResultServer.Name = "lblResultServer";
            this.lblResultServer.Size = new System.Drawing.Size(127, 13);
            this.lblResultServer.TabIndex = 1;
            this.lblResultServer.Text = "Сообщение от сервера:";
            // 
            // btnStartProcess
            // 
            this.btnStartProcess.Location = new System.Drawing.Point(12, 12);
            this.btnStartProcess.Name = "btnStartProcess";
            this.btnStartProcess.Size = new System.Drawing.Size(116, 45);
            this.btnStartProcess.TabIndex = 2;
            this.btnStartProcess.Text = "Запуск процесса 1 на сервере";
            this.btnStartProcess.UseVisualStyleBackColor = true;
            this.btnStartProcess.Click += new System.EventHandler(this.btnStartProcess_Click);
            // 
            // btnStartSever
            // 
            this.btnStartSever.Location = new System.Drawing.Point(134, 12);
            this.btnStartSever.Name = "btnStartSever";
            this.btnStartSever.Size = new System.Drawing.Size(116, 45);
            this.btnStartSever.TabIndex = 3;
            this.btnStartSever.Text = "Запустить сервер";
            this.btnStartSever.UseVisualStyleBackColor = true;
            this.btnStartSever.Click += new System.EventHandler(this.btnStartSever_Click);
            // 
            // ClientOne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 150);
            this.Controls.Add(this.btnStartSever);
            this.Controls.Add(this.btnStartProcess);
            this.Controls.Add(this.lblResultServer);
            this.Controls.Add(this.txtResult);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ClientOne";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Клиент № 1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblResultServer;
        private System.Windows.Forms.Button btnStartProcess;
        private System.Windows.Forms.Button btnStartSever;
        public System.Windows.Forms.TextBox txtResult;
    }
}
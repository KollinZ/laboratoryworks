﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab_1
{
    public partial class task2 : Form
    {
        public task2()
        {
            InitializeComponent();
        }

/// <summary>
/// Метод задает начертание полужирный "bold"
/// </summary>
private void cbBold_CheckedChanged(object sender, EventArgs e)
{
    txtFontText.Font = cbBold.Checked ? new Font(txtFontText.Font, FontStyle.Bold) : new Font(txtFontText.Font, FontStyle.Regular);
}

/// <summary>
/// Метод задает начертание наклонный "italic"
/// </summary>
private void cbItalic_CheckedChanged(object sender, EventArgs e)
{
    txtFontText.Font = cbItalic.Checked ? new Font(txtFontText.Font, FontStyle.Italic) : new Font(txtFontText.Font, FontStyle.Regular);
}

/// <summary>
/// Метод задает шрифт "Arial"
/// </summary>
private void btnArial_Click(object sender, EventArgs e)
{
    txtFontText.Font = new Font("Arial", txtFontText.Font.Size);
}

/// <summary>
/// Метод задает шрифт "Symbol"
/// </summary>
private void btnSymbol_Click(object sender, EventArgs e)
{
    txtFontText.Font = new Font("Symbol", txtFontText.Font.Size);
}

/// <summary>
/// Метод задает шрифт "Times New Romman"
/// </summary>
private void btnTimeNewRomman_Click(object sender, EventArgs e)
{
    txtFontText.Font = new Font("Times New Romman", txtFontText.Font.Size);
}

/// <summary>
/// Метод задает шрифт "Courier New"
/// </summary>
private void btnCourierNew_Click(object sender, EventArgs e)
{
    txtFontText.Font = new Font("Courier New", txtFontText.Font.Size);
}
    }
}

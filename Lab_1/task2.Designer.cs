﻿namespace Lab_1
{
    partial class task2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFontText = new System.Windows.Forms.TextBox();
            this.lblFontText = new System.Windows.Forms.Label();
            this.lblFontSelect = new System.Windows.Forms.Label();
            this.cbBold = new System.Windows.Forms.CheckBox();
            this.cbItalic = new System.Windows.Forms.CheckBox();
            this.btnArial = new System.Windows.Forms.Button();
            this.btnSymbol = new System.Windows.Forms.Button();
            this.btnTimeNewRomman = new System.Windows.Forms.Button();
            this.btnCourierNew = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtFontText
            // 
            this.txtFontText.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtFontText.Location = new System.Drawing.Point(12, 33);
            this.txtFontText.Name = "txtFontText";
            this.txtFontText.ReadOnly = true;
            this.txtFontText.Size = new System.Drawing.Size(315, 80);
            this.txtFontText.TabIndex = 0;
            this.txtFontText.Text = "DemoFont";
            // 
            // lblFontText
            // 
            this.lblFontText.AutoSize = true;
            this.lblFontText.Location = new System.Drawing.Point(107, 7);
            this.lblFontText.Name = "lblFontText";
            this.lblFontText.Size = new System.Drawing.Size(81, 13);
            this.lblFontText.TabIndex = 1;
            this.lblFontText.Text = "Шрифт текста:";
            // 
            // lblFontSelect
            // 
            this.lblFontSelect.AutoSize = true;
            this.lblFontSelect.Location = new System.Drawing.Point(125, 157);
            this.lblFontSelect.Name = "lblFontSelect";
            this.lblFontSelect.Size = new System.Drawing.Size(96, 13);
            this.lblFontSelect.TabIndex = 2;
            this.lblFontSelect.Text = "Выберите шрифт:";
            // 
            // cbBold
            // 
            this.cbBold.AutoSize = true;
            this.cbBold.Location = new System.Drawing.Point(99, 127);
            this.cbBold.Name = "cbBold";
            this.cbBold.Size = new System.Drawing.Size(47, 17);
            this.cbBold.TabIndex = 3;
            this.cbBold.Text = "Bold";
            this.cbBold.UseVisualStyleBackColor = true;
            this.cbBold.CheckedChanged += new System.EventHandler(this.cbBold_CheckedChanged);
            // 
            // cbItalic
            // 
            this.cbItalic.AutoSize = true;
            this.cbItalic.Location = new System.Drawing.Point(174, 127);
            this.cbItalic.Name = "cbItalic";
            this.cbItalic.Size = new System.Drawing.Size(48, 17);
            this.cbItalic.TabIndex = 4;
            this.cbItalic.Text = "Italic";
            this.cbItalic.UseVisualStyleBackColor = true;
            this.cbItalic.CheckedChanged += new System.EventHandler(this.cbItalic_CheckedChanged);
            // 
            // btnArial
            // 
            this.btnArial.Location = new System.Drawing.Point(12, 179);
            this.btnArial.Name = "btnArial";
            this.btnArial.Size = new System.Drawing.Size(142, 58);
            this.btnArial.TabIndex = 5;
            this.btnArial.Text = "Arial";
            this.btnArial.UseVisualStyleBackColor = true;
            this.btnArial.Click += new System.EventHandler(this.btnArial_Click);
            // 
            // btnSymbol
            // 
            this.btnSymbol.Location = new System.Drawing.Point(12, 243);
            this.btnSymbol.Name = "btnSymbol";
            this.btnSymbol.Size = new System.Drawing.Size(142, 58);
            this.btnSymbol.TabIndex = 6;
            this.btnSymbol.Text = "Symbol";
            this.btnSymbol.UseVisualStyleBackColor = true;
            this.btnSymbol.Click += new System.EventHandler(this.btnSymbol_Click);
            // 
            // btnTimeNewRomman
            // 
            this.btnTimeNewRomman.Location = new System.Drawing.Point(185, 179);
            this.btnTimeNewRomman.Name = "btnTimeNewRomman";
            this.btnTimeNewRomman.Size = new System.Drawing.Size(142, 58);
            this.btnTimeNewRomman.TabIndex = 7;
            this.btnTimeNewRomman.Text = "Times New Romman";
            this.btnTimeNewRomman.UseVisualStyleBackColor = true;
            this.btnTimeNewRomman.Click += new System.EventHandler(this.btnTimeNewRomman_Click);
            // 
            // btnCourierNew
            // 
            this.btnCourierNew.Location = new System.Drawing.Point(185, 243);
            this.btnCourierNew.Name = "btnCourierNew";
            this.btnCourierNew.Size = new System.Drawing.Size(142, 58);
            this.btnCourierNew.TabIndex = 8;
            this.btnCourierNew.Text = "Courier New";
            this.btnCourierNew.UseVisualStyleBackColor = true;
            this.btnCourierNew.Click += new System.EventHandler(this.btnCourierNew_Click);
            // 
            // task2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 307);
            this.Controls.Add(this.btnCourierNew);
            this.Controls.Add(this.btnTimeNewRomman);
            this.Controls.Add(this.btnSymbol);
            this.Controls.Add(this.btnArial);
            this.Controls.Add(this.cbItalic);
            this.Controls.Add(this.cbBold);
            this.Controls.Add(this.lblFontSelect);
            this.Controls.Add(this.lblFontText);
            this.Controls.Add(this.txtFontText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "task2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Задача № 2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFontText;
        private System.Windows.Forms.Label lblFontText;
        private System.Windows.Forms.Label lblFontSelect;
        private System.Windows.Forms.CheckBox cbBold;
        private System.Windows.Forms.CheckBox cbItalic;
        private System.Windows.Forms.Button btnArial;
        private System.Windows.Forms.Button btnSymbol;
        private System.Windows.Forms.Button btnTimeNewRomman;
        private System.Windows.Forms.Button btnCourierNew;
    }
}
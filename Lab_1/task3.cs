﻿using System;
using System.Windows.Forms;

namespace Lab_1
{
    public partial class task3 : Form
    {
        /// <summary>
        /// Число загаданный случайным образом
        /// </summary>
        public int UnknownNumber;

        /// <summary>
        /// Количество неудачных попыток
        /// </summary>
        public int FailedAttemptsCount = 0;

        public task3()
        {
            InitializeComponent();
            Rand();
        }

        /// <summary>
        /// Метод задает случайное число от 1 до 100
        /// </summary>
        private void Rand()
        {
            Random rand = new Random();
            UnknownNumber = rand.Next(1, 100);
            Text = UnknownNumber.ToString(); //TODO
        }

        /// <summary>
        /// Метод хранит логику игры
        /// </summary>
        private void RunGame()
        {
            int specifiedNumber = Convert.ToInt32(txtNumber.Text);

            if (specifiedNumber < 1 || specifiedNumber > 100)
            {
                MessageBox.Show("Загадайте число от 1 до 100 включительно.", "Неверный ввод",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumber.Clear();
                return;
            }

            if (specifiedNumber == UnknownNumber)
            {
                picBox.Image = Properties.Resources.smile1;
                MessageBox.Show("Вы угадали, игра закончена!\nКоличество попыток: " + FailedAttemptsCount, "Информация",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                Rand();
                FailedAttemptsCount = 0;
            }
            else if (specifiedNumber < UnknownNumber)
            {
                picBox.Image = Properties.Resources.smile2;
                MessageBox.Show("Ваше число меньше...", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                FailedAttemptsCount++;
            }
            else
            {
                picBox.Image = Properties.Resources.smile2;
                MessageBox.Show("Ваше число больше...", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                FailedAttemptsCount++;
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            RunGame();
        }

        private void txtNumber_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtNumber.Text, "[^0-9]"))
            {
                MessageBox.Show("Пожалуйста, введите только цифры");
                txtNumber.Text = txtNumber.Text.Remove(txtNumber.Text.Length - 1);
            }
            btnRun.Enabled = !string.IsNullOrEmpty(txtNumber.Text);
            picBox.Image = null;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                RunGame();
            }
        }
    }
}
